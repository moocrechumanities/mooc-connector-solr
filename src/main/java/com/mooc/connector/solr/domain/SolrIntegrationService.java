package com.mooc.connector.solr.domain;


import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mooc.connector.solr.application.DTO.response.SolrResponseDTO;
import com.mooc.connector.solr.infrastructure.ExternalProperties;

import ch.qos.logback.classic.Logger;

@Service
public class SolrIntegrationService {

	private Logger logger = (Logger) LoggerFactory.getLogger(SolrIntegrationService.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ExternalProperties externalProperties;

	public SolrResponseDTO getResultfromSolr(String data) {

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		HttpEntity<String> request = new HttpEntity<String>(headers);

		ResponseEntity<String> response = null;

		logger.info(data);

		String url = externalProperties.getBaseSolrUrl() + "content:" + data + " OR title:" + data + "&rows="
				+ externalProperties.getMaxLimit();

		try {

			logger.info("START RETRIEVING SOLR DATA : IN PROGRESS");

			response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

		} catch (RestClientException e) {
			
			logger.info("START RETRIEVING SOLR DATA : CRASHED");

			throw new RestClientException("DATA PROCCESSING EXCEPTION");

		}

		return (response != null) ? convertJSONToObject(response, SolrResponseDTO.class) : null;
	}

	public <T> T convertJSONToObject(ResponseEntity<String> response, Class<T> responseType) {

		T t = null;

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		try {

			t = objectMapper.readValue(response.getBody().toString(), responseType);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return t;
	}

	public <T> String convertObjectToJson(T t) {

		String output = null;
		com.fasterxml.jackson.databind.ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		try {
			output = objectMapper.writeValueAsString(t);
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage(), e);
		}

		return output;
	}

}
