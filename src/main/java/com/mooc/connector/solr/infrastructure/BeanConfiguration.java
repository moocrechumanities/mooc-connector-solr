package com.mooc.connector.solr.infrastructure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class BeanConfiguration {

	@Value("${resttemplate.readtimeout}")
	private String readTimeout;
	
	@Value("${resttemplate.connecttimeout}")
	private String connectTimeout;
	
	@Bean
	public RestTemplate restTemplate() {
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setReadTimeout(Integer.parseInt(readTimeout));
        requestFactory.setConnectTimeout(Integer.parseInt(connectTimeout));
		return new RestTemplate(requestFactory);
	}
	
}
