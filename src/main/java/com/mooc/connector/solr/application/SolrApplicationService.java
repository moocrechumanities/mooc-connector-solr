package com.mooc.connector.solr.application;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mooc.connector.solr.application.DTO.SolrConnectorSearchResponseDataDTO;
import com.mooc.connector.solr.application.DTO.response.SolrResponseDTO;
import com.mooc.connector.solr.application.transformer.SolrTransformer;
import com.mooc.connector.solr.domain.SolrIntegrationService;

import ch.qos.logback.classic.Logger;

@Service
public class SolrApplicationService {
	
	private Logger logger = (Logger) LoggerFactory.getLogger(SolrApplicationService.class);

	@Autowired
	SolrIntegrationService solrIntegrationService;

	@Autowired
	SolrTransformer solrTransformer;

	public SolrConnectorSearchResponseDataDTO searchInSolr(String data) {

		SolrResponseDTO solrResponseDTO = solrIntegrationService.getResultfromSolr(customizeSpaceSeparatedString(data));

		logger.info("START RETRIEVING SOLR DATA : COMPLETED");
		
		return solrTransformer.toSolrConnectorSearchResponseDTO(solrResponseDTO);

	}

	private String customizeSpaceSeparatedString(String data) {

		if (data.contains(" ")) {
			data = data.replace(" ","\\ ");
		}
		return data;
	}

}
