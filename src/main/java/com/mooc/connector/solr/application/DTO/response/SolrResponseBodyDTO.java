package com.mooc.connector.solr.application.DTO.response;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SolrResponseBodyDTO {

	private int numFound;
	private int start;
	private ArrayList<SolrResponseBodyDocs> docs;

	public int getNumFound() {
		return numFound;
	}

	public void setNumFound(int numFound) {
		this.numFound = numFound;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public ArrayList<SolrResponseBodyDocs> getDocs() {
		return docs;
	}

	public void setDocs(ArrayList<SolrResponseBodyDocs> docs) {
		this.docs = docs;
	}


}
