package com.mooc.connector.solr.application.DTO;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SolrConnectorSearchResponseDataDTO {

	private List<SolrConnectorSearchResponseDTO> data;

	public List<SolrConnectorSearchResponseDTO> getData() {
		return data;
	}

	public void setData(List<SolrConnectorSearchResponseDTO> data) {
		this.data = data;
	}


	
}
