package com.mooc.connector.solr.application.DTO.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SolrHeaderResponseDTO {

	private int status;
	private int Qtime;
	private SolrResponseHeaderParamsDTO params;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getQtime() {
		return Qtime;
	}

	public void setQtime(int qtime) {
		Qtime = qtime;
	}

	public SolrResponseHeaderParamsDTO getParams() {
		return params;
	}

	public void setParams(SolrResponseHeaderParamsDTO params) {
		this.params = params;
	}

}
