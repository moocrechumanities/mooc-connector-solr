package com.mooc.connector.solr.application.DTO.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SolrResponseBodyDocs {

	private String id;
	
	private List<String> courseId;
	
	private List<String> title;
	
	private List<String> content;
	
	private List<String> type;
	
	private List<String> url;
	
	private List<String> courseSource;

	@JsonProperty("_version_")
	private Long versionId;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getCourseId() {
		return courseId;
	}

	public void setCourseId(List<String> courseId) {
		this.courseId = courseId;
	}

	public List<String> getTitle() {
		return title;
	}

	public void setTitle(List<String> title) {
		this.title = title;
	}

	public List<String> getContent() {
		return content;
	}

	public void setContent(List<String> content) {
		this.content = content;
	}

	public List<String> getType() {
		return type;
	}

	public void setType(List<String> type) {
		this.type = type;
	}

	public List<String> getUrl() {
		return url;
	}

	public void setUrl(List<String> url) {
		this.url = url;
	}

	public Long getVersionId() {
		return versionId;
	}

	public void setVersion(Long version) {
		this.versionId = version;
	}

	public List<String> getCourseSource() {
		return courseSource;
	}

	public void setCourseSource(List<String> courseSource) {
		this.courseSource = courseSource;
	}



}
