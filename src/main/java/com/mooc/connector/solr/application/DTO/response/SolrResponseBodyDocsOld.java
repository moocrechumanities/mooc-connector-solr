package com.mooc.connector.solr.application.DTO.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SolrResponseBodyDocsOld {
	
	@JsonProperty("course_id")
	private List<String> courseId;

	@JsonProperty("module_id")
	private List<String> moduleId;

	@JsonProperty("sub_module_id")
	private List<String> subModuleId;

	private List<String> title;
	private List<String> content;
	private String id;

	public List<String> getCourseId() {
		return courseId;
	}

	public void setCourseId(List<String> courseId) {
		this.courseId = courseId;
	}

	public List<String> getModuleId() {
		return moduleId;
	}

	public void setModuleId(List<String> moduleId) {
		this.moduleId = moduleId;
	}

	public List<String> getSubModuleId() {
		return subModuleId;
	}

	public void setSubModuleId(List<String> subModuleId) {
		this.subModuleId = subModuleId;
	}

	public List<String> getTitle() {
		return title;
	}

	public void setTitle(List<String> title) {
		this.title = title;
	}

	public List<String> getContent() {
		return content;
	}

	public void setContent(List<String> content) {
		this.content = content;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
