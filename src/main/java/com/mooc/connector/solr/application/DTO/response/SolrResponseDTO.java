package com.mooc.connector.solr.application.DTO.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SolrResponseDTO {

	private SolrHeaderResponseDTO responseHeader;
	private SolrResponseBodyDTO response;

	public SolrHeaderResponseDTO getResponseHeader() {
		return responseHeader;
	}

	public void setResponseHeader(SolrHeaderResponseDTO responseHeader) {
		this.responseHeader = responseHeader;
	}

	public SolrResponseBodyDTO getResponse() {
		return response;
	}

	public void setResponse(SolrResponseBodyDTO response) {
		this.response = response;
	}

}
