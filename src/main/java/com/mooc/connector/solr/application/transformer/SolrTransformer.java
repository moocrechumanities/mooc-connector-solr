package com.mooc.connector.solr.application.transformer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.mooc.connector.solr.application.DTO.SolrConnectorSearchResponseDTO;
import com.mooc.connector.solr.application.DTO.SolrConnectorSearchResponseDataDTO;
import com.mooc.connector.solr.application.DTO.response.SolrResponseBodyDocs;
import com.mooc.connector.solr.application.DTO.response.SolrResponseDTO;

@Service
public class SolrTransformer {

	public List<SolrConnectorSearchResponseDTO> toSolrConnectorSerachResponseList(SolrResponseDTO solrResponseDTO) {

		List<SolrConnectorSearchResponseDTO> solrSearchRequestList = new ArrayList<SolrConnectorSearchResponseDTO>();

		List<SolrResponseBodyDocs> solrResponseBodyDocsList = solrResponseDTO.getResponse().getDocs();

		for (int i = 0; i < solrResponseBodyDocsList.size(); i++) {

			SolrConnectorSearchResponseDTO solrConnectorSearchResponse = new SolrConnectorSearchResponseDTO();

			solrConnectorSearchResponse.setId(solrResponseBodyDocsList.get(i).getId());
			solrConnectorSearchResponse.setCourseId(solrResponseBodyDocsList.get(i).getCourseId().get(0) != null
					? solrResponseBodyDocsList.get(i).getCourseId().get(0)
					: null);
			solrConnectorSearchResponse.setTitle(solrResponseBodyDocsList.get(i).getTitle().get(0) != null
					? solrResponseBodyDocsList.get(i).getTitle().get(0)
					: null);
//			solrConnectorSearchResponse.setContent(solrResponseBodyDocsList.get(i).getContent().get(0) != null
//					? solrResponseBodyDocsList.get(i).getContent().get(0)
//					: null);
			solrConnectorSearchResponse.setType(solrResponseBodyDocsList.get(i).getType().get(0) != null
					? solrResponseBodyDocsList.get(i).getType().get(0)
					: null);
			solrConnectorSearchResponse.setUrl(solrResponseBodyDocsList.get(i).getUrl().get(0) != null
					? solrResponseBodyDocsList.get(i).getUrl().get(0)
					: null);
			solrConnectorSearchResponse.setCourseSource(solrResponseBodyDocsList.get(i).getCourseSource().get(0) != null
					? solrResponseBodyDocsList.get(i).getCourseSource().get(0)
					: null);
			solrConnectorSearchResponse.setVersionId(solrResponseBodyDocsList.get(i).getVersionId());

			solrSearchRequestList.add(solrConnectorSearchResponse);
		}

		return solrSearchRequestList;

	}
	
	public SolrConnectorSearchResponseDataDTO toSolrConnectorSearchResponseDTO(SolrResponseDTO solrResponseDTO) {
		
		SolrConnectorSearchResponseDataDTO  solrConnectorSearchResponseDataDTO = new SolrConnectorSearchResponseDataDTO();
		
		solrConnectorSearchResponseDataDTO.setData(toSolrConnectorSerachResponseList(solrResponseDTO));
		
		return solrConnectorSearchResponseDataDTO;
	}

//	public List<SolrConnectorSearchResponse> toSolrConnectorSerachResponseListOld(SolrResponseDTO solrResponseDTO) {
//
//		List<SolrConnectorSearchResponse> solrSearchRequestList = new ArrayList<SolrConnectorSearchResponse>();
//		
//
//		List<SolrResponseBodyDocs> solrResponseBodyDocsList = solrResponseDTO.getResponse().getDocs();
//
//		for (int i = 0; i < solrResponseBodyDocsList.size(); i++) {
//
//			System.out.println(solrResponseBodyDocsList.get(i).getContent().get(0));
//			SolrConnectorSearchResponse solrSearchRequest = new SolrConnectorSearchResponse();
//	
//			solrSearchRequest.setId(solrResponseBodyDocsList.get(i).getId());
//			solrSearchRequest.setCourseId(solrResponseBodyDocsList.get(i).getCourseId().get(0));
//			solrSearchRequest.setModuleId(solrResponseBodyDocsList.get(i).getModuleId().get(0));
//			solrSearchRequest.setSubModuleId(solrResponseBodyDocsList.get(i).getSubModuleId().get(0));
//			solrSearchRequest.setTitle(solrResponseBodyDocsList.get(i).getTitle().get(0));
//			solrSearchRequest.setContent(solrResponseBodyDocsList.get(i).getContent().get(0));
//
//			solrSearchRequestList.add(solrSearchRequest);
//		}
//
//		return solrSearchRequestList;
//
//	}
}
