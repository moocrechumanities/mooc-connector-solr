package com.mooc.connector.solr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mooc.connector.solr.application.SolrApplicationService;
import com.mooc.connector.solr.application.DTO.SolrConnectorSearchResponseDataDTO;

@RestController
public class SolrRouteController {

	@Autowired
	SolrApplicationService solrApplicationService;

	@RequestMapping(value = "search", method = RequestMethod.GET)
	public SolrConnectorSearchResponseDataDTO searchInsolr(@RequestParam(name = "data") String data) {

		return solrApplicationService.searchInSolr(data);

	}

}
