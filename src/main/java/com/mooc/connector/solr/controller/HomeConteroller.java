package com.mooc.connector.solr.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeConteroller {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Object sample() {
		return new String("WELCOME TO SPRING BOOT APPLICATION");
	}
}
