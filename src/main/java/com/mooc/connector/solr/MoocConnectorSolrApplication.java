package com.mooc.connector.solr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoocConnectorSolrApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoocConnectorSolrApplication.class, args);
	}

}
